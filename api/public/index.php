<?php
/**
 * @name Archivo Index del Panel
 * @site EasyPanel
 * @author Easy Panel Team <easypanel@outlook.com>
 * @copyright 2014
 */
 
 //Muestra de errores
 error_reporting(E_ALL);
 ini_set("display_errors", 1);
 
 //Define Constantes de URL y PATH del sitio
 define( "ABS_PATH" , str_replace(array(DIRECTORY_SEPARATOR,"public_","public","///***///"), array("/","///***///","","public_"), dirname( __FILE__ ) ) );
 define( "ABS_URL" , "http://".$_SERVER['HTTP_HOST'].str_replace("public/index.php", "", $_SERVER['PHP_SELF']));
 
 //Iniciación del Motor Easy
 require_once(ABS_PATH . 'sys/engine.php');
 
 
 global $route;
 
 //Ruteo de la peticion
 $route = route::getInstance();
 $route->dispatch();
 
?>