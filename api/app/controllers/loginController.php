<?php

class loginController extends BaseController{

	public function __construct(){
		parent::__construct();
		
	}	
	
	public function indexAction(){

		if(isset($_POST['user'],$_POST['pass'])){
			if($this->app->user->login($_POST['user'],$_POST['pass'])){
				$this->printMsg("LOGED");
			}else{
				$this->printError("INVALID_AUTH");
			}
			
		}else{
		    if(!isset($_GET['help']))
		      $this->printError("NO_FLDS");
		}
        
        if(isset($_GET['help'])){
            echo "<h1>Login</h1>Toma 2 variables por POST, las cuales son: <strong>user, pass</strong>. <br />";
            echo "<h3>Resultado</h3>";
            echo "El resultado puede traer:<br />";
            echo "LOGED: Usuario logueado correctamente<br />";
            echo "<strong><u>ERRORES</u></strong><br/>";
            echo "NO_FLDS: No se recibieron todas las variables requeridas.<br />";
            echo "INVALID_AUTH: Usuario o contraseña Invalidas.";
            
        }
	}
	
	
}