<?php

class productosController extends BaseController{
        
        private $model; 
        
        public function __construct(){
            $this->model = new productosModel();
        }
        
        public function indexAction(){
            
        }
        
        public function sliderAction(){
            
            $this->printMsg("",$this->model->getSliderProducts());
            
        }
        
        
        public function ultimosAction(){
            $this->printMsg("",$this->model->getLastProducts());
        }
        
        public function tiposAction(){
            
            $this->printMsg("",$this->model->getTiposProductos());
            
        }
}
    