<?php

class clientesController extends BaseController{

    private $model;
    private $security;

    public function __construct(){
        parent::__construct();
        
        $this->model = new clientesModel();
        $this->security = security::getInstance();
    }   
    
    public function indexAction(){  
        
    }
    
    public function logueadoAction(){
        if($this->app->user->is_loged()){
            $this->printMsg("LOGGED");
        }else{
            $this->printError("NOT-LOGGED");
        }
    }
    
    public function datosAction(){
        if($this->app->user->is_loged()){
            $data = array();
            $data['nombre'] = $this->app->user->get_nombre();
            $this->printMsg("LOGGED",$data);
        }else{
            $this->printError("NOT-LOGGED");
        }
    }
    
    public function newAction(){
        
        $datos = array("nombre","nickname","email","pass","direccion","ciudad","cpostal","telefono","celular","nombretarjeta","numerotarjeta","codigotarjeta","vencimientotarjeta");
        
        if(!empty($_POST['nombre']) && !empty($_POST['nickname']) && !empty($_POST['email']) && !empty($_POST['pass']) && !empty($_POST['direccion']) && !empty($_POST['ciudad']) && isset($_POST['cpostal']) 
        && !empty($_POST['telefono']) && !empty($_POST['celular']) && !empty($_POST['nombretarjeta']) && !empty($_POST['numerotarjeta']) && !empty($_POST['codigotarjeta']) && !empty($_POST['vencimientotarjeta'])){
            
            //Comprueba si ya hay un cliente registrado con el email
            if(!$this->model->isUserRegistered()){
                
                //Contraseña de usuario
                if($this->app->user->conf['encriptacion'])
                    $_POST['pass'] = $this->security->prep_encr($_POST['pass'],$this->app->user->conf['encType']);
                                
                //Registra usuario
                $resultados = $this->model->registrarUsuario();
                if($resultados[0] && $resultados[1]){
                    $this->printMsg("USR_REGIST");
                }else{
                    if(!$resultados[0])
                        $this->printError("ERR_BD");
                    
                    if(!$resultados[1])
                        $this->printMsg("ERR_BD_TARJ");
                }
                
            }else{
                $this->printError("USR_YA_REGISTRADO_MAIL");
            }
            
        }else{
            if(!isset($_GET['help']))
                $this->printError("NO_FLDS");
        }
        
        if(isset($_GET['help'])){
            echo "<h1>Registrar nuevos usuarios</h1>Toma ".count($datos)." variables por POST, las cuales son: <strong>".implode(", ",$datos)."</strong>. <br />";
            echo "<i>El unico que no es Obligatorio (puede ser vacio) es: ".$datos[6]."</i>";
            echo "<h3>Resultado</h3>";
            echo "El resultado puede traer:<br />";
            echo "USR_REGIST: Usuario registrado correctamente<br />";
            echo "ERR_BD_TARJ: Usuario registrado, pero la tarjeta no pudo ser guardada.<br />";
            echo "<strong><u>ERRORES</u></strong><br/>NO_FLDS: No se recibieron todas las variables requeridas.<br />";
            echo "USR_YA_REGISTRADO_MAIL: El usuario ya esta registrado (validacion por mail de los usuarios)<br />";
            echo "ERR_BD: Hubo un error al guardar los datos en la BD.";
            
        }
    }
    
    public function modifyAction(){
        
        $datos = array("nombre","email","pass","direccion","ciudad","cpostal","telefono","celular","nombretarjeta","numerotarjeta","codigotarjeta","vencimientotarjeta");
        
        if(!empty($_POST['nombre']) && !empty($_POST['email']) && isset($_POST['pass']) && !empty($_POST['direccion']) && !empty($_POST['ciudad']) && isset($_POST['cpostal']) 
        && !empty($_POST['telefono']) && !empty($_POST['celular']) && !empty($_POST['nombretarjeta']) && !empty($_POST['numerotarjeta']) && !empty($_POST['codigotarjeta']) &&
         !empty($_POST['vencimientotarjeta'])){
            
            //Comprueba si el usuario tiene la sesion iniciada
            if($this->app->user->is_loged()){
                
                //Contraseña de usuario
                if(!empty($_POST['pass'])){
                    if($this->app->user->conf['encriptacion'])
                        $_POST['pass'] = $this->security->prep_encr($_POST['pass'],$this->app->user->conf['encType']);
                }
                                
                //Registra usuario
                $resultados = $this->model->modificarUsuario();
                if($resultados[0] && $resultados[1]){
                    $this->printMsg("USR_MOD");
                }else{
                    if(!$resultados[0])
                        $this->printError("ERR_BD");
                    
                    if(!$resultados[1])
                        $this->printMsg("ERR_BD_TARJ");
                }
                
            }else{
                $this->printError("NO_USR");
            }
            
        }else{
            if(!isset($_GET['help']))
                $this->printError("NO_FLDS");
        }
        
        if(isset($_GET['help'])){
            echo "<h1>Modificar usuarios</h1>Toma ".count($datos)." variables por POST, las cuales son: <strong>".implode(", ",$datos)."</strong>. <br />";
            echo "<i>Los unicos que no son Obligatorios (pueden ser vacio) son: ".$datos[5]." y ".$datos[3]."</i>";
            echo "El nickname no se puede cambiar";
            echo "<h3>Resultado</h3>";
            echo "El resultado puede traer:<br />";
            echo "USR_MOD: Usuario Modificado correctamente<br />";
            echo "ERR_BD_TARJ: Usuario modificado, pero la tarjeta no pudo ser guardada.<br />";
            echo "<strong><u>ERRORES</u></strong><br/>NO_FLDS: No se recibieron todas las variables requeridas.<br />";
            echo "ERR_BD: Hubo un error al guardar los datos en la BD.";
            echo "NO_USR: El usuario no inicio sesion o la sesion ha caducado.";
            
        }
        
    }
    
    
}