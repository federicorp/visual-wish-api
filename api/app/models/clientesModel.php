<?php


class clientesModel extends BaseModel{
        
        
    public function isUserRegistered(){
        $result = FALSE;
        
        $sql = $this->app->dbmanager->contar("Cliente","WHERE Cliente_email='".$this->app->dbmanager->clean($_POST['email'])."'");
        
        if($sql>0){
            $result = TRUE;
        }
        
        return $result;
    }
    
    
    public function registrarUsuario(){
        $result = array(false,false);
        
        $sql = $this->db->put("Cliente_nom,Cliente_email,Cliente_dir,Cliente_cdad,Cliente_cpod,Cliente_telf,Cliente_cel,Cliente_nick,Cliente_pass",
                                "Cliente",
                                array($_POST['nombre'],$_POST['email'],$_POST['direccion'],$_POST['ciudad'],$_POST['cpostal'],$_POST['telefono'],
                                $_POST['celular'],$_POST['nickname'],$_POST['pass']));
        
        if($sql!==FALSE){
            $result[0] = TRUE;
            $id_user = $this->db->fetch($this->db->get("Cliente_id as id","Cliente","WHERE Cliente_email='?'",$_POST['email']),"row");
            
            $sql = $this->db->put("Cliente_id,Tarj_nro,Tarj_marca,Tarj_cod",
                                "TarjetaCredito",
                                array($id_user[0],$_POST['numerotarjeta'],$_POST['nombretarjeta'],$_POST['codigotarjeta']));
            if($sql!==FALSE){
                $result[1] = TRUE;
            }
        }
        
        return $result;
    }
    
    public function modificarUsuario(){
        $result = array(false,false);
        
        $campos = array("Cliente_nom","Cliente_email","Cliente_dir","Cliente_cdad","Cliente_cpod","Cliente_telf","Cliente_cel","Cliente_nick");
        $valores = array($_POST['nombre'],$_POST['email'],$_POST['direccion'],$_POST['ciudad'],$_POST['cpostal'],$_POST['telefono'],$_POST['celular'],$_POST['nickname']);
        
        if(!empty($_POST['pass'])){
            $campos[] = "Cliente_pass";
            $valores[] = $_POST['pass'];
        }
        
        $sql = $this->db->mod($campos,
                                $valores,
                                "Cliente",
                                "WHERE Cliente_id=".$this->db->clean($this->app->user->get_id()));
        
        if($sql!==FALSE){
            $result[0] = TRUE;
            
            $sql = $this->db->mod(array("Tarj_nro","Tarj_marca","Tarj_cod"),
                                array($_POST['numerotarjeta'],$_POST['nombretarjeta'],$_POST['codigotarjeta']),
                                "TarjetaCredito",
                                "WHERE Cliente_id=".$this->db->clean($this->app->user->get_id()));
            if($sql!==FALSE){
                $result[1] = TRUE;
            }
        }
        
        return $result;
    }
    
    
}
