<?php


class productosModel extends BaseModel{
    
    public function __construct(){
        parent::__construct();
    }

    public function getSliderProducts(){
        
        $result = false;
        
        $sql = $this->db->get(
        "p.Prod_Id AS id, p.Prod_nombre AS nombre, p.Prod_fecha AS fecha, p.Prod_precio AS precio, p.Prod_desc AS descripcion, p.Prod_tam AS size, 
        t.Tipo_id AS idTipo, t.Tipo_nom AS nomTipo, p.Prod_cant AS cant, fp.Foto_url AS urlImg",
        "( Productos p INNER JOIN TipoProductos t ON ( (p.Tipo_id = t.Tipo_id) )) LEFT JOIN FotoProductos fp ON ( ( p.Prod_id = fp.Prod_id ) ) ",
        "GROUP BY p.Prod_Id ORDER BY p.Prod_fecha DESC , p.Prod_Id DESC LIMIT 3");
        
        if($sql !== FALSE){
            $result = array();
            while($row = $this->db->fetch($sql,"assoc")){
                $result[] = $row;
            }
        }
        
        return $result;
        
    }
    
    public function getLastProducts(){
        
        $result = false;
        
        $sql = $this->db->get(
        "p.Prod_Id AS id, p.Prod_nombre AS nombre, p.Prod_fecha AS fecha, p.Prod_precio AS precio, p.Prod_desc AS descripcion, p.Prod_tam AS size, 
        t.Tipo_id AS idTipo, t.Tipo_nom AS nomTipo, p.Prod_cant AS cant, fp.Foto_url AS urlImg",
        "( Productos p INNER JOIN TipoProductos t ON ( (p.Tipo_id = t.Tipo_id) )) LEFT JOIN FotoProductos fp ON ( ( p.Prod_id = fp.Prod_id ) ) ",
        "GROUP BY p.Prod_Id ORDER BY p.Prod_fecha DESC , p.Prod_Id DESC LIMIT 3,8");
        
        if($sql !== FALSE){
            $result = array();
            while($row = $this->db->fetch($sql,"assoc")){
                $result[] = $row;
            }
        }
        
        return $result;
        
    }
    
    public function getTiposProductos(){
        
        $result = false;
        
        $sql = $this->db->get("Tipo_id as id,Tipo_nom as nombre","TipoProductos");
        
        if($sql !== FALSE){
            $result = array();
            while($row = $this->db->fetch($sql,"assoc")){
                $result[] = $row;
            }
        }
        
        return $result;
        
    }

}        
?>
    