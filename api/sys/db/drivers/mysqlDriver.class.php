<?php

/**
 * Archivo de funciones mysqli, recibe como parámetros constructores la direccion  el archivo de configuracion.
 * Con las constantes: 
 * HOSTSQL,USUARIOSQL,PASSSQL,DBSQL
 * 
 * @name Clase MySQLi
 * @package classPHP
 * @author Federicorp
 * @copyright 2013 Federicorp
 * @license http://opensource.org/licenses/gpl-license.php GNU Public License  
 * 
 */

 class mysqlDriver{
     
     private $app;
     
     private $campos=array();
     private $tabla="";
     
     private $consulta_grupo = NULL;
     private $grupo_array = array();
     protected $link = NULL;
     protected $archivo = "";
     
     
     protected $datos_con = array();
     
     /**
      * Construye un objeto del tipo mysql con mysqli
      * 
      * @param $archivo
      *
      * 
      */
     
     
     public function __construct(){
         
          
     }
     
     public function __set($name,$value){
         if(strpos($name, "con_")>-1){
             $this->datos_con[str_replace("con_", "", $name)] = $value; 
         }
     }
     
     public function get_link(){
         return $this->link;
     }
     
     public function testConn(){
         global $errorhandler;
         
         $this->link = mysqli_connect($this->datos_con["host"],$this->datos_con["user"],$this->datos_con["pass"],$this->datos_con["db"]);
         if($this->link==false)
            $errorhandler->showHTMLError('Error al establecer la conexión a la base de datos. <br />'.mysqli_error($this->link),true);
     }
    
      
     
     /**
      * 
      * Selecciona la base de datos, sin parámetros elije la base de datos predeterminada.
      * @param $nombre
      * 
      */
     
     public function select_bd($nombre=""){
         if(!defined(DBSQL))
            require_once $this->archivo;
         if($nombre==""){
             if(!mysqli_select_db($this->link,DBSQL))
                 show_error_html("Error al seleccionar la base de datos.".mysqli_error($this->link),true);
         }else{
             if(!mysqli_select_db($this->link,$nombre)){
                 if(!mysqli_select_db($this->link,DBSQL)){
                    show_error_html("Error al seleccionar la base de datos.".mysqli_error($this->link),true);
                 }else{
                     show_error_html("No se ha podido seleccionar la base de datos '".$nombre."'. Se ha seleccionado la BD por defecto",(__LINE__)-4);
                 }
             }
         }
     }
     
     /**
      * Retorna el resultado del query mysql.
      * ATENCIÓN: Sanear las entradas del usuario.
      * 
      *  @param $sql
      *  @return mysqli resource
      */
     
     public function query($sql){
         //echo $sql;
         return mysqli_query($this->link,$sql);
     }
     
     /**
      * Consulta insert con entradas saneadas
      * 
      * @param $campos, $tabla, $condicion
      * @return mysqli resource
      */
     
     
     public function put($campos,$tabla,$valores){
         $sql = "INSERT INTO ".$tabla;
         $sql .= "($campos) VALUES(";
         foreach ($valores as &$value) {
             $value = "'" . $this->clean($value) . "'";
         }
         $sql .= implode(",", $valores);
         $sql .= ");";
         return $this->query($sql);
     }
     
     /**
      * Consulta select con entradas saneadas
      * 
      * @param $campos, $tabla, $condicion
      * @return mysqli resource
      */
     
     
     public function get($campos,$tabla,$condicion=""){
         $args = func_get_args();
         $args_count = func_num_args()-3;
         $sql = "SELECT ".$campos;
         $sql .= " FROM ".$tabla;
         $c = 3;
         if($condicion!=""){
             if($args_count == substr_count($condicion, "?")){
                 $exploded = explode("?", $condicion);
                 foreach ($exploded as $value) {
                     if($c==3)
                        $sql .= " ";
                     if(!isset($args[$c])){
                         $sql .= $value;
                     }else{
                         $sql .= $value . $this->clean($args[$c]);
                     }
                     $c++;
                 }
             }
         }
         
         //echo $sql;
         
         return $this->query($sql);
     }
     
     /**
      * Consulta update con entradas saneadas
      * 
      * @param $campos, $tabla, $condicion
      * @return mysqli resource
      */
     
     
     public function mod($campos,$valores,$tabla,$condicion=""){
         $args = func_get_args();
         $args_count = func_num_args()-4;
         $sql = "UPDATE ".$tabla . " SET ";
         for($i = 0;$i<count($campos);$i++){
             if($i>0){
                 $sql .= ",";
             }
             $sql .= $campos[$i]."='".$this->clean($valores[$i])."'";
         }
         $c = 4;
         if($condicion!=""){
             if($args_count == substr_count($condicion, "?")){
                 $exploded = explode("?", $condicion);
                 foreach ($exploded as $value) {
                     if($c==4)
                        $sql .= " ";
                     if(!isset($args[$c])){
                         $sql .= $value;
                     }else{
                         $sql .= $value . $this->clean($args[$c]);
                     }
                     $c++;
                 }
             }
         }
         
         return $this->query($sql);
     }
     
     
     /**
      * Retorna el último error mysql
      * 
      *  @param none
      *  @return mysqli_error
      */
     
     public function get_error(){
         return mysqli_error($this->link);
     }
     
     /**
      * Retorna Saneado el String.
      * 
      *  @param $sql
      *  @return string
      */
     
     public function clean($string){
         return mysqli_real_escape_string($this->link,$string);
     }
     
     
     /**
      * Retorna el COUNT(*) de la tabla con la condicion proporcionada
      * ATENCIÓN: Sanear el String $condicion.
      * 
      *  @param $tabla,$condicion
      *  @return int
      */
     
     public function contar($tabla,$condicion=""){
         //echo "SELECT COUNT(*) FROM ".$tabla." ".$condicion;
         $row = mysqli_fetch_array($this->query("SELECT COUNT(*) FROM ".$tabla." ".$condicion),MYSQLI_NUM);
         return $row[0];
     }
     
     /**
      * Retorna el fetch de una consulta realizada anteriormente
      * Posibles valores para $mode
      * "array","assoc","row"
      * "array" por default
      * 
      *  @param $mysql resource, $mode
      *  @return array
      */
     
     public function fetch($mysql,$mode="array"){
        if($mode=="array"){
            return mysqli_fetch_array($mysql);
        }else if($mode=="assoc"){
            return mysqli_fetch_array($mysql,MYSQLI_ASSOC);
        }else if($mode=="row"){
            return mysqli_fetch_array($mysql,MYSQLI_NUM);
        }
     }
     
     /**
      * Retorna un tag 'select' HTML con los valores de la consulta enviada
      * 
      * 
      *  @param $mysql sentence,$nombre_select,$campo_valor,$campo_mostrar,$attr_select
      *  @return string
      */
     
     public function cargar_combo($sql,$nombre_select,$campo_valor,$campo_mostrar,$selected_value="",$attr_select=""){
         $resultado = "<select name='".$nombre_select."' ".$attr_select.">";
         $result = $this->query($sql);
         while($row = $this->fetch($result,"assoc")){
             $valor = "";
             if(htmlspecialchars($row[$campo_mostrar], ENT_QUOTES,'UTF-8')==""){
                 $valor = str_replace('"','/"',str_replace("'","/'",$row[$campo_mostrar]));
             }else{
                 $valor = htmlspecialchars($row[$campo_mostrar], ENT_QUOTES);
             }
             if($row[$campo_valor]==$selected_value){
                 $resultado .= '<option value="'.$row[$campo_valor].'" selected="selected">'.$valor.'</option>';
             }else{
                 $resultado .= '<option value="'.$row[$campo_valor].'">'.$valor.'</option>';
             }
         }
         return $resultado."</select>";
     }
     
     public function agregar_campo_consulta(){
         $args = func_get_args();
         $last = "";
         $c=0;
         foreach ($args as $value) {
             if($c%2!=0){
                 $this->campos[$last] = $value;
             }else{
                 $this->campos[$value] = "";
                 $last=$value;
             }
             $c++;
         }
     }
     
     public function agregar_tabla($tabla){
         $this->tabla = $tabla;
     }
     
     public function guardar_campos($condicion="",$mode=false,$erase_anyways=true){
         if($mode==false){
             $campo = array();
             $datos = array();
             foreach ($this->campos as $key => $value) {
                 array_push($campo,$key);
                 array_push($datos,$value);
             }
             $sql_text = "INSERT INTO ".$this->tabla."(";
             for($i=0; $i<count($campo);$i++) {
                 if($i!=0){
                     $sql_text .= ",";
                 }
                 $sql_text .= $campo[$i];
             }
             $sql_text .= ") VALUES(";
             for($i=0; $i<count($datos);$i++) {
                 if($i!=0){
                     $sql_text .= ",";
                 }
                 $sql_text .= "'".$this->clean($datos[$i])."'";
             }
             $sql_text .= ")";
         }else{
             $sql_text = "UPDATE ".$this->tabla." SET ";
             $c=0;
             foreach ($this->campos as $key => $value) {
                 if($c!=0){
                     $sql_text .= ",";
                 }
                 $sql_text .= $key."='".$this->clean($value)."'";
                 $c++;
             }
             $sql_text .= $condicion;
         }
         if($this->consulta_grupo==true && $mode==false){
             $this->grupo_array[] = array("tabla_modificacion_sistema"=>$this->tabla);
             $this->grupo_array[count($this->grupo_array)-1] = array_merge($this->grupo_array[count($this->grupo_array)-1],$this->campos);
         }
         $this->campos=array();
         $this->tabla = "";
         if($erase_anyways){
            $this->campos=array();$this->tabla = "";
         }
         if(!$this->query($sql_text))
             return false;
         
         $this->campos=array();
         $this->tabla = "";
         
         return true;
     }

    /**
     * Define un grupo de consultas en caso de haber un error al agregar las anteriores. Elimina todo nuevamente.
     * 
     */

    public function set_grupo_consultas(){
        $this->consulta_grupo = true;
    }
    
    /**
     * Termina el grupo de consultas.
     */
    
    public function end_grupo_consultas(){
        $this->consulta_grupo = NULL;
    }
    
    /**
     * Borra el grupo en caso de haber un error en la ejecución de alguna consulta MySQL
     */
    
    public function borrar_grupo(){
        for ($i=count($this->grupo_array)-2; $i>-1 ; $i--) {
            $sql_text = "DELETE FROM ".$this->grupo_array[$i]['tabla_modificacion_sistema']." WHERE ";
            $c=0;
            foreach ($this->grupo_array[$i] as $key => $value) {
                if($key!="tabla_modificacion_sistema"){
                    if($c!=0){
                        $sql_text .= " AND ";
                    }
                    $sql_text .= $key."='".$this->clean($value)."'";
                    $c++;
                }
            }
            if(!$this->query($sql_text))
                show_error_html("Error borrando el grupo de consultas.".$this->error(),true);
            $this->consulta_grupo = NULL;
            $this->grupo_array = array();
        }
    }
    
    /**
     * Obtiene el valor de un campo X de una consulta realizada previamente.
     */
    
    public function get_campo_from_grupo($pos,$campo){
        $sql_text = "SELECT * FROM ".$this->grupo_array[$pos]['tabla_modificacion_sistema']." WHERE ";
        $c=0;
        foreach ($this->grupo_array[$pos] as $key => $value) {
            if($key!="tabla_modificacion_sistema"){
                if($c!=0){
                    $sql_text .= " AND ";
                }
                $sql_text .= $key."='".$this->clean($value)."'";
                $c++;
            }
        }
        $result = $this->fetch($this->query($sql_text));
        return $result[$campo];
    }
    
    
    /**
     * Listar Checkbox sin las opciones marcadas
     */
     
    public function listar_checkbox($sentencia_sql,$campo_value,$campo_mostrar){
        $sql = $this->query($sentencia_sql);
        $c=0;
        echo "<table class='lista check_boxs'>";
        while($row = $this->fetch($sql)){
            if($c==0){
                echo "<tr>";
            }
            $c++;
            echo '<td style="text-align:left;" title="'.$row[$campo_mostrar].'" class="tooltip-js"><div style="display:inline;"><input type="checkbox" name="chk_'.$row[$campo_value].'" id="chk_'.$row[$campo_value].'" /></div><span>'.recortar_texto($row[$campo_mostrar],50).'</span></td>';
            if($c==2){
                $c=0;
                echo "</tr>";
            }
        }
        echo "</table>";
    }

    /**
     * Mostrar checkbox con opciones marcadas
     * 
     */
     
    public function mostrar_checkbox($sentencia_sql, $tabla_comparar, $campo_mostrar, $campo_value, $campo_comparar, $campo_comparar_valor, $valor){
        $sql = $this->query($sentencia_sql);
        $c=0;
        echo "<table class='lista check_boxs'>";
        while($row = $this->fetch($sql)){
            if($c==0){
                echo "<tr>";
            }
            $c++;
            $checked = 'checked="checked"';
            if(!$this->contar($tabla_comparar,"WHERE ".$campo_comparar."='".$row[$campo_value]."' AND ".$campo_comparar_valor."='".$valor."'")>0){
                $checked = "";
            }
            
            echo '<td style="text-align:left;" title="'.$row[$campo_mostrar].'" class="tooltip-js"><div style="display:inline;"><input type="checkbox" name="chk_'.$row[$campo_value].'" id="chk_'.$row[$campo_value].'" '.$checked.'  /></div><span>'.recortar_texto($row[$campo_mostrar],50).'</span></td>';
            
            if($c==2){
                $c=0;
                echo "</tr>";
            }
        }
        echo "</table>";
    }

    /*
     * Guarda los valores del checkbox con tablas de dos columnas
     */
     
    public function guardar_checkbox($tabla_referencia,$tabla_guardar,$columna_reemplazar,$valor_reemplazar,$columna_valor){
            
        //Backup en caso de fallo
        $sqlBCK = $this->query("SELECT * FROM ".$tabla_guardar." WHERE ".$columna_reemplazar."=".$valor_reemplazar);
        
        $this->query("DELETE FROM ".$tabla_guardar." WHERE ".$columna_reemplazar."=".$valor_reemplazar);
        $sql = $this->query("SELECT * FROM ".$tabla_referencia) or die($this->get_error());
        while($row = $this->fetch($sql)){
            if(@$_POST['chk_'.$row[0]]=="on"){
                if(!$this->query("INSERT INTO ".$tabla_guardar." (".$columna_valor.",".$columna_reemplazar.") VALUES('".$row[0]."','".$valor_reemplazar."')")){
                    $this->rest_checkbox($sqlBCK,$tabla_guardar,$columna_valor,$columna_reemplazar);
                    return false; 
                }
            }
        }
        return true;
    }
    
    private function rest_checkbox($sqlBCK,$tabla,$columna_valor,$columna_reemplazar){
        $sql = "INSERT INTO ".$tabla." (".$columna_valor.",".$columna_reemplazar.") VALUES";
        $c=0;
        while($row = $this->fetch($sqlBCK,"assoc")){
            var_dump($row);
            if($c>0){
                $sql .= ",";
            }else{
                $c++;
            }
            $sql .= "('".$row[$columna_valor]."','".$row[$columna_reemplazar]."')";
        }
        $this->query($sql);
    }
    
    
    public function close(){
        mysqli_close($this->link);
    }
    
    
     /**
       * Singleton
       */
      private static $instancia;
     
     public static function getInstance()
     {
          if (  !self::$instancia instanceof self)
          {
             self::$instancia = new self();
          }
          return self::$instancia;
     }
     public function __clone()
       {
          trigger_error("Operación Invalida: No puedes clonar una instancia de ". get_class($this) ." class.", E_USER_ERROR );
       }
       public function __wakeup()
       {
          trigger_error("No puedes deserializar una instancia de ". get_class($this) ." class.");
       }
 }


?>