<?php

/**
 * Clase de Handler de Errores
 * 
 * @name Clase ErrorHandler
 * @package classPHP
 * @author Easy Panel Team <easypanel@outlook.com>, Federico Romero <federicorp@easypanel.com>
 * @copyright 2014
 * 
 */
 
 class errorhandler{
 	
	 private $debugging;
     
     public function showHTMLError($error,$fatal=false){
     	if($this->debugging!==TRUE)
			return ;
         if($fatal==true){
            if(!headers_sent())
                die('<html lang="es"><head><meta charset="utf-8" /><title>Error Fatal</title><style>body{font-family:"Arial","Helvetica";}</style></head><body><div style="margin: 0 auto;max-width: 500px;background: #8E0000; color:#FFF;text-align: center;padding: 10px;border: solid thin #F0F0F0">'.$error.'</div></body></html>');
            die('<div style="margin: 0 auto;max-width: 500px;background: #8E0000; color:#FFF;text-align: center;padding: 10px;border: solid thin #F0F0F0;">'.$error.'</div>');
        }else{
            echo "<div style='margin: 0 auto;max-width: 500px;background: #C1E0FF;text-align: center;padding: 10px;border: solid thin #F0F0F0'>".$error."</div>";
        }
     }
	 
	 public function setStateDebug($debugging){
	 	$this->debugging = $debugging; 
	 }
     
 }
 
 ?>