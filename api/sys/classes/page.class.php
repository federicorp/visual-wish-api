<?php

/**
 * Archivo para inclusion de páginas requeridas
 * 
 * @name Clase Paginas
 * @package classPHP
 * @author Easy Panel Team <easypanel@outlook.com>, Federico Romero <federicorp@easypanel.com>
 * @copyright 2014
 * 
 */
 
 class page{
     
     //Librerias a utilizar
     private $app = null;     
     
     private $js_engine = false;
     
     private $paginaActual = "";
     
     private $triggers = array(
     
         "beforeLoad"=>array(),
         "afterLoad"=>array()
     
     );
     
     private $vars = array();
     
     
    public function __construct(){
        
    }
    
    public function setLibrary(){
        $this->app = app::getInstance();
    }
    
    
    //Funcion para incluir las páginas
    
    public function dispatcher(){
        global $route;
        
        /**
         * Validaciones correspondientes a las páginas
         * Formato:
         * Validación{
         *     Se recuperan los permisos que debe tener el usuario para ver la página.
         * }
         */
         
         $_url = $route->controller . "/" . $route->action . "/" . $route->data;
         
         $datosPage = FALSE;
         
        /*// Consulta a la base de datos para obtener el permiso para la pagina
        $mysql = $this->app->dbmanager->get("permId,pagTitulo,pagDescripcion", "paginas","WHERE pagDireccion='?'",$_url);
        if($row = $this->app->dbmanager->fetch($mysql)){
            $datosPage['permiso'] = $row['permId'];
            
            $this->setVar("page_titulo", $row['pagTitulo']);
            $this->setVar("page_descripcion", $row['pagDescripcion']);
        }
        
        //Validar que no se ejecuten a parte las otras páginas
        $validar = true;

        if($datosPage !== FALSE){
            //Si requiere de permisos entra aquí
            //Se comprueban los permisos
            if(!$this->permisos->comprobacion($permisos)){
                $this->noPerm();
            }
            
        }*/
        
        $this->controllerExec();
        
        $this->viewInc();
        
        
    }

    //Usuario no tiene permisos para ver la página
    private function noPerm(){
        $this->app->errorHandler->showHTMLError("No tiene permisos para ver la página",true);
    }
    
    private function controllerExec(){
        global $route;
        
        $controller = $route->controller;
        $action = ($route->action!="")?$route->action:"index";
        
		if($controller!="index"){
			$firstController = "index";
			$class = $firstController.'Controller';
	        if(file_exists(ABS_PATH."app/controllers/".str_replace("controller", "Controller", $class).".php")){
	            $fcontroller = new $class();
	        }
		}
		
		
        $class = $controller.'Controller';
        if(file_exists(ABS_PATH."app/controllers/".str_replace("controller", "Controller", $class).".php")){
            $controller = new $class();
            
            $action .= "Action";
            if(method_exists($controller, $action))        
                $controller->$action();
        }
        
        
    }
    
    private function viewInc($templates = FALSE, $sensitive = true){
        global $route;
        
        $controller = $route->controller;
        $action = $route->action;
        
        if($templates === FALSE){
            $templates = array($controller . "/" . $action,$controller . "_" . $action,$controller . "/index" ,$controller,$action);
        }
        
        //Enviar variable desde el controller a la vista
        foreach ($this->vars as $key => $value) {
            $$key = $value;
        }
        
        $page = $this->getInstance();
        
        $user = $this->app->user;
        
        $c = 0;
        foreach ($templates as $template) {
            
            if(file_exists($this->escape_includes(ABS_PATH . "app/views/" . $template . ".phtml"))){
            
                include_once($this->escape_includes(ABS_PATH . "app/views/" . $template . ".phtml"));
                
                $c++;
                
                break;
                
            }
            
        }
        
        if($c==0){
            if(file_exists($this->escape_includes(ABS_PATH . "app/views/index.phtml")) && $sensitive){
                include_once($this->escape_includes(ABS_PATH . "app/views/index.phtml"));
                return ;
            }
            if($sensitive)
                $this->app->errorHandler->showHTMLError("No hay vista para cargar.",true);
        }
        
    }
    
    public function setVar($name,$value){
        $this->vars[$name] = $value;
    }
    
    public function getHeader(){
        global $route;
        
        $controller = $route->controller;
        $action = $route->action;
        
        $templates = array($controller . "/" . $action . "Header",$controller . "_" . $action . "Header",$controller . "/header" ,$controller."Header",$action."Header","header");
        
        if(!isset($_GET['ajax']))
            $this->viewInc($templates,false);
        
    }
    
    public function getFooter(){
        global $route;
        
        $controller = $route->controller;
        $action = $route->action;
        
        $templates = array($controller . "/" . $action . "Footer",$controller . "_" . $action . "Footer",$controller . "/footer" ,$controller."Footer",$action."Footer","footer");
        
        if(!isset($_GET['ajax']))
            $this->viewInc($templates,false);
    }
	
	public function bgRedirect($url){
		global $route;
		$_GET['_url'] = $url;
		
		$route->setUrl();
		$route->dispatch();
	}
    
    public function is($page="index"){
        if($this->get_slug()==$page)
            return true;
        return false;
    }
	
	public function controlleris($controller="index"){
		global $route;
        if($route->controller==$controller)
            return true;
        return false;
    }
    
    public function escape_includes($include){
        return str_replace("/", DIRECTORY_SEPARATOR, $include);
    }
    
    public function includes($include){
        if(file_exists($include)){
            include_once($include);
        }else{
            die("No existe: ".$include);
        }
    }
    
    public function setPage($pagina){
        $exploded = explode(DIRECTORY_SEPARATOR, str_replace(".php", "", $pagina));
        $this->paginaActual = end($exploded);
    }
    
    public function include_styles(){
        $url = ABS_PATH."/includes/pags/css/".$this->paginaActual.".css";
        if(file_exists($url))
            return str_replace(ABS_PATH, "", $url);
        return "";
    }

    public function include_funciones(){
        $file = ABS_PATH."/funciones/funct_".$this->paginaActual.".php";
        $class = ABS_PATH."/classes/user_defined/".$this->paginaActual.".class.php";
        $b = array();
        if(file_exists($file)){
            require_once($file);
            $b[0] = "Se incluyó librería de funciones.";
        }else{
            $b[0]="No se pudo incluir librería de funciones.";
        }
        if(file_exists($class)){
            require_once($class);
            $b[1] = "Se incluyó archivo de classes.";
        }else{
            $b[1]="No se pudo incluir archivo de classes.";
        }
        return $b;
    }
    
    public function engine_js(){
        if(!$this->js_engine){
            $this->js_engine = true;
            echo '
                <!-- BEGIN JAVASCRIPTS -->
                <script src="'.ABS_URL.'/assets/vendor-js/jquery.js"></script>   
                <!--[if lt IE 9]>
                <script src="'.ABS_URL.'/assets/js/excanvas.js"></script>
                <script src="'.ABS_URL.'/assets/js/respond.js"></script>    
                <![endif]-->    
                
				
                <script type="text/javascript" src="'.ABS_URL.'/assets/jgritter/js/jquery.gritter.js"></script>
                '. /*
                <script type="text/javascript" src="'.ABS_URL.'/assets/calendar/date.js"></script>
                <script type="text/javascript" src="'.ABS_URL.'/assets/calendar/jquery.datePicker.js"></script>
				
				<script src="'.ABS_URL.'/assets/jquery-ui/jquery-ui-1.10.1.custom.min.js"></script> 
                <script src="'.ABS_URL.'/assets/jquery-slimscroll/jquery.slimscroll.min.js"></script>
				
				<!--
                <script src="'.ABS_URL.'/assets/metro/js/jquery.blockui.js"></script>
                --> 
                 
                
                <script type="text/javascript" src="'.ABS_URL.'/assets/uniform/jquery.uniform.min.js"></script> 
                <script type="text/javascript" src="'.ABS_URL.'/assets/pulsate/jquery.pulsate.min.js"></script>
				
                */ '
                <!-- Tooltip -->
                <script type="text/javascript" src="'.ABS_URL.'/assets/jquery-tooltip/jQuery.tooltip.js"></script>
                <!-- Dirty Form -->
                <script type="text/javascript" src="'.ABS_URL.'/assets/jquery-dirtyforms/jquery.dirtyforms.js"></script>
                
                <script type="text/javascript" src="'.ABS_URL.'/assets/facebox/js/facebox.js"></script>
                
                <script src="'.ABS_URL.'/assets/bootstrap/dist/js/bootstrap.min.js"></script>
				
                <script src="'.ABS_URL.'/assets/easy/js/main.js"></script>           
                <script>     
                    easyApi.init(); // init the rest of plugins and elements
                    //Tooltips
                    $("td.tooltip-js").tooltip();
                    $("a.tooltip-js").tooltip();
                </script>
                <!-- END JAVASCRIPTS -->
           ';
       }
    }
    
    public function include_js(){
        $file = ABS_PATH."/includes/pags/js/".$this->paginaActual.".js";
        if(file_exists($file)){
            if(!$this->js_engine){
                $this->engine_js();
            }
            echo "<script src='".ABS_URL."/includes/pags/js/".$this->paginaActual.".js'></script>";
        }
    }
    
    /**
     * Obtener la dirección de la página
     */
     
     public function get_URL($mode="normal"){
         switch ($mode) {
             case 'normal':
                 $url = ABS_URL."/".$_GET['p'];
                 if($GLOBALS['pagina_botones']!=0){
                     $url .= "?btn=".$GLOBALS['pagina_botones'];
                 }
                 return $url;
                 break;
             
             default:
                 return ABS_URL."/".$_GET['p'];
                 break;
         }
     }

    // Obtiene la URL actual
    public function getCurURL(){
        $url="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
        return $url;
    }

    /**
     * Hace una redireccion a una url o a una pagina dentro del panel
     */

    public function redirectTo($url,$isPage=false){
        if(headers_sent()){
            //Si las cabeceras ya han sido enviadas
            echo '<script type="text/javascript">
                        location.href="' . ABS_URL . $url . '";
                    </script>'; 
        }else{
            //Si las cabeceras aún no han sido enviadas
            header("Location: " . ABS_URL . $url);
        }
    }
    
    
    /**
     * Sistema de paginación
     */
     
     
    private function first_page($cant_muestr_par,$pagina,$cant_total_paginas,$muestra_pag,$mitad_muestra){
        //Defino con que comienza la paginación como predeterminado el numero 1
        $start = 1;
        if($cant_total_paginas>$muestra_pag){
            //Si la cantidad de páginas es mayor a la que se muestra, entra en esta condicion
            if($pagina-($mitad_muestra-1) > 1){
                //Si la página es mayor a la mitad entra en esta condición
                $start = $pagina-($mitad_muestra-1);
                //Cantidad de la otra mitad
                $mit_comp=$mitad_muestra;
                if(!$cant_muestr_par){
                    //Si la cantidad de paginas es impar entonces se resta uno a la cantidad de paginas de la otra mitad
                    $mit_comp=$mitad_muestra-1;
                }
                $resta_pag = 0;
                if($pagina+$mit_comp>$cant_total_paginas){
                    $resta_pag = ($pagina+$mit_comp)-$cant_total_paginas;
                }
                $start -= $resta_pag;
            }
        }
        return $start;
    }
    
    private function end_page($start,$cant_total_paginas,$muestra_pag){
        if(($start+($muestra_pag-1))<$cant_total_paginas){
            return $start+($muestra_pag-1);
        }else{
            return $cant_total_paginas;
        }
    }
    
    private function mostrar_inicio($start,$pagina,$link_pag){
        if($start > 1){
            echo "<li><a href='".$link_pag."1'>Primera</li><li><a href='".$link_pag.($pagina-1)."'>&laquo;</a></li>";
        }else{
            if($pagina != 1){
                echo "<li><a href='".$link_pag.($pagina-1)."'>&laquo;</a></li>";
            }
        }
    }
    
    private function mostrar_fin($end,$cant_total_paginas,$pag_active,$link_pag,$pagina){
        if($end<$cant_total_paginas){
            echo "<li><a href='".$link_pag.($pagina+1)."'>&raquo;</a></li><li><a href='".$link_pag.$cant_total_paginas."'>&Uacute;ltima</a></li>";
        }else{
            if($end!=$pag_active){
                echo "<li><a href='".$link_pag.($pagina+1)."'>&raquo;</a></li>";
            }
        }
    }
    
    private $tabla_pag = "";
    private $condicion = "";
    private $items_por_pag = 0;
    private $pagina = 1;
    private $palabra = "";
    
    public function set_paginacion($campos,$tabla,$condicion="",$palabra="page",$items_por_pag=10){
        $this->tabla_pag = $tabla;
        $this->condicion = $condicion;
        $this->items_por_pag = $items_por_pag;
        $this->palabra = $palabra;
        
        if(isset($_GET[$palabra])){
            if(is_numeric($_GET[$palabra]) && $_GET[$palabra]>0){
                //si hay una variable 'page' en la URL se toma si es numerico
                $this->pagina = $_GET[$palabra];
            }
        }
        
        $rango_ini = ($this->items_por_pag * $this->pagina) - $this->items_por_pag;
        /* Uso del LIMIT N,M
        N= desde que registro
        M= cantidad de registros a mostrar
        */
        
        return $this->mysql->get($campos,$this->tabla_pag,"LIMIT " . $rango_ini . "," . $this->items_por_pag);
        
    }
    
    public function show_paginacion($pag_mostradas=20){
        //Cantidad de páginas
        $counter_paginas = $this->mysql->contar($this->tabla_pag);
        $cant_total_paginas = round_up($counter_paginas / $this->items_por_pag); 
        //Cantidad de páginas a mostrarse en la paginación. (No se recomienda poner menos de 3 páginas, debido a que se han detectado bugs)
        $muestra_pag = $pag_mostradas;
        //link de la paginación (sin la variable de paginación que le será agregado al final)
        $link_trim = explode("&", $_SERVER['REQUEST_URI']);
        foreach ($link_trim as $key => $value) {
            if(strpos($value, $this->palabra)!==FALSE){
                unset($link_trim[$key]);
            }
        }
        
        $link_pag = implode("&", $link_trim);
        //Si ya posee variables de URL el link active esta bandera
        $link_var = false;
        if(strpos($_SERVER['REQUEST_URI'], "?")!==FALSE){
            $link_var = true;
        }        
        
        
        //Forma el link de la paginaci�n
        if($link_var){
            $link_pag .= "&page=";
        }else{
            $link_pag .= "?page=";
        }
        
        //Mitad de la cantidad de muestras
        $mitad_muestra = round_up($muestra_pag/2);
        //Defino si es par o impar la cantidad de muestras
        $cant_muestr_par = false;
        if($muestra_pag%2==0){
                $cant_muestr_par = true;
        }
        
        //Obtener en que número comenzará la paginación
        $start = $this->first_page($cant_muestr_par,$this->pagina,$cant_total_paginas,$muestra_pag,$mitad_muestra);
        //Obtener en que número finalizará la paginación
        $end = $this->end_page($start,$cant_total_paginas,$muestra_pag);
        if($cant_total_paginas!=0){
            echo "<div class='pagination'>
                        <ul>";
            
            //Muestra &lacuo y link hacia la Primera página
            $this->mostrar_inicio($start,$this->pagina,$link_pag);
            $pag_active = 0;
            //Paginaci�n por medio de for
            for($i=$start;$i<=$end;$i++){
                if($this->pagina != $i){
                    echo "<li><a href='".$link_pag.$i."' class='btn btn-default'>".$i."</a></li>";
                }else{
                    echo "<li class='active'><a href='#' class='btn btn-default'>".$i."</a></li>";
                    $pag_active = $i;
                }
            }
            
            //Muestra &raquo y Link hacia la �ltima p�gina
            $this->mostrar_fin($end,$cant_total_paginas,$pag_active,$link_pag,$this->pagina);
            echo "</ul>
                </div>";
        }
    }
     
    
        /**
       * Singleton
       */
        private static $instancia;
         
        public static function getInstance(){
            if (  !self::$instancia instanceof self){
                self::$instancia = new self();
            }
            return self::$instancia;
        }
        public function __clone(){
            trigger_error("Operación Invalida: No puedes clonar una instancia de ". get_class($this) ." class.", E_USER_ERROR );
        }
        public function __wakeup()
        {
            trigger_error("No puedes deserializar una instancia de ". get_class($this) ." class.");
        }
 }

?>