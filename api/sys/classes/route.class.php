<?php

/**
 * Clase de Ruteo
 * 
 * @name Clase Ruteo
 * @package classPHP
 * @author Easy Panel Team <easypanel@outlook.com>, Federico Romero <federicorp@easypanel.com>
 * @copyright 2014
 * 
 */
 
 
 class route{
     
     public $controller;
     
     public $action;
     
     public $data;
     
     protected $app;
     
     
     public function __construct(){
         
         $this->app = app::getInstance();
         
         $this->setUrl();
         
     }
	 
	 public function setUrl(){
	 	// Obtención de URL para controllers, actions y data.
         $urlExpl = explode("/", (isset($_GET['_url']))?$_GET['_url']:"/index/");
         
         $this->controller = (isset($urlExpl[1]))? $urlExpl[1] : "";
         
         $this->action = (isset($urlExpl[2]))? $urlExpl[2] : "";
         
         $this->data = (isset($urlExpl[3]))? $urlExpl[3] : "";  
	 }
     
     public function dispatch(){
         $this->app->page->dispatcher();
     }
     
     /**
     * Singleton
     */
     
    private static $instancia;
    
    public static function getInstance(){
         if (  !self::$instancia instanceof self){
            self::$instancia = new self;
         }
         return self::$instancia;
    }
    
    public function __clone(){
       trigger_error("Operación Invalida: No puedes clonar una instancia de ". get_class($this) ." class.", E_USER_ERROR );
    }
    
    public function __wakeup(){
       trigger_error("No puedes deserializar una instancia de ". get_class($this) ." class.");
    }
     
 }
 
 ?>