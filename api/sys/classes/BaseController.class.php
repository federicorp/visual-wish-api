<?php


class BaseController{
    
	public $app;
	public $route;
    
    public function __construct(){
        global $app,$route;
		
		$this->route = $route;
        $this->app = $app;
    }
    
	
	public function indexAction(){
		
	}
	
	public function printError($message,$data=array()){
		echo json_encode(array("resultado"=>"ERROR","mensaje"=>$message,"datos"=>$data));
		exit();
	}
	
	public function printMsg($message,$data=array()){
		echo json_encode(array("resultado"=>"OK","mensaje"=>$message,"datos"=>$data));
		exit();
	}
	
}


?>