<?php
/**
 * @name Clase del Tracker del Sitio
 * @package classPHP
 * @author Federicorp
 * @copyright 2013 Federicorp
 * @license http://opensource.org/licenses/gpl-license.php GNU Public License  
 * 
 */
 
 class tracker{
     
     protected $app;
     
     private $user = null;
     
     
     public function __construct(){
         
         
     }
     
     public function setLibrary(){
        $this->app = app::getInstance();
    }
     
     
     //Registro del Tracker
     public function register($tipo, $descripcion){
        //se obtiene la fecha y hora del evento
        $date = getFechaHora();
        //Se obtiene la descripción, escapando con MySQL
        $descripcion = $this->user->mysql->clean($descripcion);
        //Con que navegador se ha realizado el evento
        $config = configuraciones_nav();
        //Se agrega a la descripción las configuraciones del navegador del lado cliente
        $descripcion .= $this->user->mysql->clean(". La acción se ha hecho con las siguientes configuraciones '" .
            $config . "'");
        //Se insertan en la tabla auditoria los datos
        if ($this->user->mysql->query("INSERT INTO tracker(fecha_tracker,ip_tracker,descrip_tracker,fk_id_acciones_tracker,fk_id_usuario,fk_id_pagina) VALUES('" .
            $date . "','" . getRealIP() . "','" . $descripcion . "',1," . $this->user->get_id() .
            ",1)")) {
                //Exitoso retorna true
            return true;
        } else {
            //Error retorna false
            return false;
        }
    }
         
     
     
     
     
     /**
       * Singleton
       */
      private static $instancia;
     
     public static function getInstance()
     {
          if (  !self::$instancia instanceof self)
          {
             self::$instancia = new self;
          }
          return self::$instancia;
     }
     public function __clone()
       {
          trigger_error("Operación Invalida: No puedes clonar una instancia de ". get_class($this) ." class.", E_USER_ERROR );
       }
       public function __wakeup()
       {
          trigger_error("No puedes deserializar una instancia de ". get_class($this) ." class.");
       }
 }
 
 ?>