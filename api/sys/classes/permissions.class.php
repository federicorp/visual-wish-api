<?php

/**
 * Archivo de funciones para Verificar permisos de usuarios
 * 
 * @name Clase Páginas
 * @package classPHP
 * @author EasyPanel Team
 * @copyright 2013  
 * 
 */
 
 class permissions{
     
     protected $app;
     
     /**
      * Configuraciones de Base de datos y usuarios
      */
      
     protected $conf_perm = array(
        
        /* -------- Tabla permisos -------- */
        
        "nombre_tabla_permiso" => "permisos",
        "id_permiso" => "id_permisos",
        
        /* -------- Tabla Perfiles -------- */
        
        "nombre_tabla_perfil" => "perfiles_permisos",
        "id_perfil" => "id_perfil",        
        
        /* -------- Tabla de Conjunción --------- */
        
        "nombre_tabla_conj" => "permisos_has_perfiles",
        "id_perfil_conj" => "fk_id_perfil",
        "id_permiso_conj" => "fk_id_permisos"
     
     );
     
     /**
      * Variable cache de permisos del usuario
      */
      
      private $cache_permisos = array();
     
     public $bool_permisos = FALSE;
     
     public $user = null;
     
     public function __construct(){
     		  
     }
     
     public function setLibrary(){
        $this->app = app::getInstance();
    }
     
     
      /**
       * Comprueba si el usuario tiene varios permisos a la vez
       * 
       * @param $permisos
       * @return boolean
       */
       
        public function comprobacion($permisos){
            if(strpos($permisos,',')==false){
                $permisos = explode(',',$permisos);
                if($permisos[0]==""){
                    $permisos[0] = $permisos[1];
                }
                if(!$this->have_permiso($permisos[0])){
                    return false;
                }else{
                    return true;
                }
            }else{
                $permisos = explode(',',$permisos);
                $count = count($permisos);
                for($i=0;$i<$count;$i++){
                    if(!$this->have_permiso($permisos[$i])){
                        return false;
                    }
                }
                return true;
            }
        }
      
      
      
     /**
      * 
      * Retorna si tiene o no permisos para realizar las acciones
      * 
      * @param $permiso
      * @return boolean
      */ 
     
     public function have_permiso($permiso){
         if(isset($this->cache_permisos[$permiso])){
             return $this->cache_permisos[$permiso];
         }else{
             $id_perfil = $this->user->get_id_pefil();
             $counter = $this->user->mysql->contar($this->conf_perm["nombre_tabla_conj"],"WHERE ".$this->conf_perm['id_permiso_conj']."='".$this->user->mysql->clean($permiso)."' AND ".$this->conf_perm["id_perfil_conj"]."='".$this->user->mysql->clean($id_perfil)."'");
             if($counter!=0){
                 $this->cache_permisos[$permiso]=TRUE;
                 return true;
             }
             $this->cache_permisos[$permiso]=FALSE;
             return false;
         }
     }
     
      /**
       * Singleton
       */
        private static $instancia;
         
        public static function getInstance(){
            if (  !self::$instancia instanceof self){
                self::$instancia = new self();
            }
            return self::$instancia;
        }
        public function __clone(){
            trigger_error("Operación Invalida: No puedes clonar una instancia de ". get_class($this) ." class.", E_USER_ERROR );
        }
        public function __wakeup()
        {
            trigger_error("No puedes deserializar una instancia de ". get_class($this) ." class.");
        }
     
 }
 
 
 ?>