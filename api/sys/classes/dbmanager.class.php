<?php

/**
 * Clase de Drivers de BD
 * 
 * @name Clase dbmanager
 * @package classPHP
 * @author Easy Panel Team <easypanel@outlook.com>, Federico Romero <federicorp@easypanel.com>
 * @copyright 2014
 * 
 */

class dbmanager{
  
    public function __construct(){
         
    }  
    
    public function getDriver(){
        
        $conf_file = ABS_PATH . "/config.inc.php";
        
        include_once $conf_file;
        
        eval('$driver = ' . strtolower(DBDRIVER) . "Driver" . '::getInstance();');
        
        $driver->con_host = HOSTSQL;
        $driver->con_user = USUARIOSQL;
        $driver->con_pass = PASSSQL;
        $driver->con_db = DBSQL;
        
        $driver->testConn();
        
        return $driver;
        
    }
    
}

?>