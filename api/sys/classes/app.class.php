<?php

/**
 * Clase de Ruteo
 * 
 * @name Clase Ruteo
 * @package classPHP
 * @author Easy Panel Team <easypanel@outlook.com>, Federico Romero <federicorp@easypanel.com>
 * @copyright 2014
 * 
 */


class app{

    public $dbmanager;
    
    public $page;
    
    public $user;
    
    public $tracker;
    
    public $permisos;
    
    public $errorHandler;
    
    public $debug;
    
    public function __construct(){
        
        $this->errorHandler = new errorhandler();
        
        $driver = new dbmanager();
        $this->dbmanager = $driver->getDriver();
        
        $this->page = page::getInstance();
        $this->user = users::getInstance();
        $this->track = tracker::getInstance();
        $this->perm = permissions::getInstance();
    }
    
    public function expandLibraries(){
        $this->page->setLibrary();
        $this->user->setLibrary();
        $this->track->setLibrary();
        $this->perm->setLibrary();
    }
    
    public function setDebugging(){
        $this->debug = true;
    }
    
    
    /**
     * Singleton
     */
     
    private static $instancia;
    
    public static function getInstance(){
         if (  !self::$instancia instanceof self){
            self::$instancia = new self;
         }
         return self::$instancia;
    }
    
    public function __clone(){
       trigger_error("Operación Invalida: No puedes clonar una instancia de ". get_class($this) ." class.", E_USER_ERROR );
    }
    
    public function __wakeup(){
       trigger_error("No puedes deserializar una instancia de ". get_class($this) ." class.");
    }
    
}

?>