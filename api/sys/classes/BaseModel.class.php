<?php

class BaseModel{
	
	public $db;
    public $app;
	
	public function __construct(){
		global $app;
        
		$this->app = $app;
		$this->db = $app->dbmanager;
	}
	
}

?>