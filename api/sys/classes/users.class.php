<?php
/**
 * @name Clase de usuarios
 * @package classPHP
 * @author Federicorp
 * @copyright 2013 Federicorp
 * @license http://opensource.org/licenses/gpl-license.php GNU Public License  
 * 
 */
 
 class users{
    
    protected $app;
    
    /* Campos a ser modificados de acuerdo a la base de datos*/
    
    public $conf = array(
        
        
        /* Encriptación de contraseña */
        
        "encriptacion" => true,
        "encType" => "md5",
        
        
        /* Variables session*/
        "nombre_session_nick" => "usuario",
        "nombre_session_pass" => "pass_usuario",
        "nombre_session_id" => "id",
        "guardar_session_pass" => false,
        
        
        /* Campos de la base de datos */
        "tabla" => "Cliente",
        "campo_id" => "Cliente_id",
        "campo_nickname" => "Cliente_nick",
        "campo_pass" => "Cliente_pass",
        "campo_mail" => ""
        
    );
    
     
     /** 
      * Nick Name del Usuario
      */
      
     private $nickname = null;
     
     
     /** 
      * Contraseña MD5 del usuario
      */
      
     private $pass = null;
     
     /** 
      * Id de la BD del Usuario
      */
     
     public $id = null;
     
     protected $security = null;
     
     public function __construct() {
        $this->includes();
        if(!isset($_SESSION))
              session_start();
        if($this->is_loged()){
            $this->nickname = $_SESSION[$this->conf['nombre_session_nick']];
            $this->id = $_SESSION[$this->conf['nombre_session_id']];
            if($this->conf["guardar_session_pass"]){
                if($row = $this->app->dbmanager->fetch($this->app->dbmanager->query("SELECT * FROM ".$this->conf["tabla"]." WHERE ".$this->conf["campo_id"]."='".$this->app->dbmanager->clean($this->id)."'")))
                    $this->pass = $row[$this->conf['nombre_session_pass']];
            }
        }
     }
     
     public function setLibrary(){
        $this->app = app::getInstance();
    }
     
     protected function includes(){
        $this->security = security::getInstance();
     }
     
     /** 
      * Funcion para el login, retorna true si los datos son correctos
      * y false si no. Setea $_SESSION['usuario'].
      * 
      *  @param nickname, contraseña
      *  @return true si los datos del usuario son correctos
      */
     
     public function login($nick,$pass){
         $pass = $this->security->prep_encr($pass,$this->conf['encType']);
         if($this->app->dbmanager->contar($this->conf["tabla"], "WHERE ".$this->conf['campo_nickname']."='".$this->app->dbmanager->clean($nick)."' AND ".$this->conf['campo_pass']."='".$this->app->dbmanager->clean($pass)."'")==1){
            $row = $this->app->dbmanager->fetch($this->app->dbmanager->query("SELECT ".$this->conf['campo_id']." FROM ".$this->conf["tabla"]." WHERE ".$this->conf['campo_nickname']."='".$this->app->dbmanager->clean($nick)."' AND ".$this->conf['campo_pass']."='".$this->app->dbmanager->clean($pass)."'"));
            $_SESSION[$this->conf['nombre_session_nick']] = $nick;
            $_SESSION[$this->conf['nombre_session_id']] = $row[$this->conf['campo_id']];
            $this->id = $row[$this->conf['campo_id']];
            return true;
         }else{
            return false;
         }
     }
     
     /**
      * Función para saber si el usuario existe en la base de datos
      * 
      * @param $id
      * @return boolean
      */
      
      public function exists($valor,$mode="id"){
          if($mode=="id"){
              if($this->app->dbmanager->contar($this->conf['tabla'], "WHERE ".$this->conf['campo_id']."=".$valor)>0)
                return true;
              return false;
          }else if($mode=="nombre"){
              if($this->app->dbmanager->contar($this->conf['tabla'], "WHERE ".$this->conf['campo_nickname']."=".$valor)>0)
                return true;
              return false;
          }else if($mode=="email"){
              if($this->app->dbmanager->contar($this->conf['tabla'], "WHERE ".$this->conf['campo_mail']."=".$valor)>0)
                return true;
              return false;
          }
      }

    /**
     * Función para saber si el pass fue reestablecido
     * 
     * @param $id
     * @return boolean
     */
     
     public function is_pass_reseted($nick){
        if($row = $this->app->dbmanager->fetch($this->app->dbmanager->get($this->conf['campo_eliminado'].",".$this->conf['campo_pass'], $this->conf['tabla'], "WHERE ".$this->conf['campo_nickname']."='?'",$nick))){
            if($row[$this->conf['campo_eliminado']]==0){
                if(strlen($row[$this->conf['campo_pass']])==6){
                    return true;
                }
            }else{
                return false;
            }
        }else{
            return false;
        }
     }
     
     
     /**
      * Función para saber si el usuario está logueado en el sistema
      * 
      *  @param none
      *  @return true si está logeado
      */
      
      public function is_loged(){
          if(isset($_SESSION[$this->conf['nombre_session_id']]))
            return true;
          return;
      }
      
      /**
      * Retorna el nombre del usuario
      * 
      * @return string
      */
      
      public function get_nombre(){
          return $this->nickname;
      }
      
      /**
      * Retorna el id del usuario
      * 
      * @return string
      */
      
      public function get_id(){
          return $this->id;
      }
      
      
      /**
       * Retrona el Id del Perfil al que corresponde un usuario
       * 
       * @param $id
       * @return int
       */
      
      public function get_id_pefil($id=0){
          if($id == 0){
             $id = $this->app->dbmanager->fetch($this->app->dbmanager->get($this->conf['campo_id_perfil'], $this->conf['tabla'],"WHERE ".$this->conf['campo_id']."='?'",$this->get_id()));
             return $id[$this->conf['campo_id_perfil']]; 
          }else{
             $id = $this->app->dbmanager->fetch($this->app->dbmanager->get($this->conf['campo_id_perfil'], $this->conf['tabla'],"WHERE ".$this->conf['campo_id']."='?'",$id));
             return $id[$this->conf['campo_id_perfil']];
          }
      }
      
      /**
      * Cierra la sesión
      * 
      * @return string
      */
      
      public function cerrar(){
          unset($_SESSION[$this->conf["nombre_session_nick"]], $_SESSION[$this->conf["nombre_session_id"]], $_SESSION[$this->conf["nombre_session_pass"]]);
      }
      
     /**
      * Singleton
      */
      
     private static $instancia;
     
     public static function getInstance(){
          if (  !self::$instancia instanceof self){
             self::$instancia = new self;
          }
          return self::$instancia;
     }
     
     public function __clone(){
        trigger_error("Operación Invalida: No puedes clonar una instancia de ". get_class($this) ." class.", E_USER_ERROR );
     }
     
     public function __wakeup(){
        trigger_error("No puedes deserializar una instancia de ". get_class($this) ." class.");
     }
 }