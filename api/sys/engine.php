<?php
/**
 * @name Archivo de Motor de Easy Panel
 * @site Easy Panel
 * @author Easy Panel Team <easypanel@outlook.com>, Federico Romero <federicorp@easypanel.com>
 * @copyright 2014
 */
 
 
//  ****    Inclusion de archivos    ****
 
 $validar = true;
 
 
 

//  ****    Error Handler    ****
  
    include_once( ABS_PATH . "sys/classes/errorhandler.class.php");
    $errorhandler = new errorhandler();



//  ****    Autoload de Clases (Librerias)    ****

  
    function autoloader($class){
        global $errorhandler;
        
        $dir = ABS_PATH."sys/classes/".$class.".class.php";
		
		//Controllers
        if(strpos(strtolower($class), "controller")>1 && $class != "BaseController"){
            $dir = ABS_PATH."app/controllers/".str_replace("controller", "Controller", $class).".php";
        }
		
		//Models
        if(strpos(strtolower($class), "model")>1 && $class != "BaseModel"){
            $dir = ABS_PATH."app/models/".str_replace("model", "Model", $class).".php";
        }
		
		//Classes definidas por el usuario
		if(strpos(strtolower($class), "class")>1){
            $dir = ABS_PATH."app/classes/".str_replace("class", "Class", $class).".php";
        }
		
		//Classes definidas por el usuario
		if(strpos(strtolower($class), "driver")>1){
            $dir = ABS_PATH."sys/db/drivers/".str_replace("driver", "Driver", $class).".class.php";
        }
	
        if(file_exists($dir)){
            require_once($dir);
        }else{
            $errorhandler->showHTMLError("No se ha podido cargar la librería ".$class.".class.php, por favor verifique que se encuentre en la carpeta correcta.",true);
        }
    }
     
    spl_autoload_register("autoloader");
 
 
 
//  ****    Comprueba si el archivo de configuración existe    ****
 
 if(!file_exists(ABS_PATH."/config.inc.php"))
    $errorhandler->showHTMLError("Aún no se ha configurado la conexión para el sitio. Configurelas y vuelva a intentar",true);

 
//  ****    Inclusión de las librerías a usarse     ****
 global $app;
 
 $app = app::getInstance();
 
 $app->expandLibraries();
 
 
?>